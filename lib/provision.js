#!/usr/bin/env node

var http = require('http');
var url = require('url');
var request = require('request');
var readline = require('readline');
var uuid = require('uuid');
var ddoc = require('./node.couchapp.js');


const SYSTEM_DB_NAME = 'system';
const ADMIN_USERNAME = 'admin';

/*
 * Provision a per-user database
 *
 * The daemon does the following:
 *   
 *   1. Creates a new user in the _users database.
 *   2. Generates a database name.
 *   3. Creates the database.
 *   4. Adds a _design/security ddoc with a validate_doc_update function that only
 *      allows the user to read and write the db.
 *   5. Puts the database name into the user's record in the _users db.
 *
 * Returns a 403 if the username already exists or the username or password is missing
 *
 * Start the daemon with "couchdb-provision [config-section]" where config-namespace
 * is the section in the CouchDB config containing the provisioning configuration.
 *
 * TODO configuration reference
 */

var CONFIG = {};
var section = process.argv[2];

var log = function(mesg) {
  console.log(JSON.stringify(["log", mesg]));
}

var do_post_or_put = function(url, req, resolve, reject, info, isPost) {
  var options = {
    url: url,
    headers: { 'Content-Type': 'application/json' },
    auth: {
      user: CONFIG[section].admin_username,
      pass: CONFIG[section].admin_password,
      sendImmediately : true
    }
  };

  req && (options.body = JSON.stringify(req));
  
  request[isPost ? 'post': 'put'](options, function(error, response, body) {
    if (error) {
      info && (error.info = info);
      reject(error);
      return;
    }
    
    var b = JSON.parse(response.body);
    if (!b.ok) {
      info && (b.info = info);
      reject(b);
    }
    else {
      resolve(b);
    }
  });
}

var do_put = function(url, req, resolve, reject, info) {
  do_post_or_put.call(this, url, req, resolve, reject, info, false);
}

var do_post = function(url, req, resolve, reject, info) {
  do_post_or_put.call(this, url, req, resolve, reject, info, true);
}

var create_user = function(data) {
  return new Promise(function(resolve, reject) {
    if (!data || !data.username || !data.password) {
      reject({ error: 'missing required parameters' });
      return;
    }
    
    var user = {
      name: data.username,
      password: data.password,
      type: 'user',
      roles: []
    };
    
    // set the namespace data for this application
    var dbname = [];
    if (CONFIG[section].add_namespace_to_dbname) {
      var dbformat = CONFIG[section].db_name_format;
      if (dbformat === 'ns_user') {
        dbname.push(CONFIG[section].namespace);
        dbname.push(data.username);
      } else if (dbformat === 'user_ns') {
        dbname.push(data.username);
        dbname.push(CONFIG[section].namespace);
      }
    } else {
      dbname.push(data.username);
    }
    if (CONFIG[section].append_uuid_to_db_name) dbname.push(uuid.v4());
    
    user[CONFIG[section].namespace] = {
      dbname: dbname.join(CONFIG[section].use_dash_in_db_name  ? '-' : '_')
    };
    
    do_put(
      'http://' + CONFIG['httpd'].bind_address + ':' + CONFIG['httpd'].port + '/'+CONFIG['couch_httpd_auth'].authentication_db+'/org.couchdb.user:' + data.username,
      user,
      function(resp) { resolve(user) },
      reject,
      'create user'
    );
  });
}

var create_database = function(user) {
  const base_url = 'http://' + CONFIG['httpd'].bind_address + ':' + CONFIG['httpd'].port + '/' + user[CONFIG[section].namespace].dbname;
  return Promise.all([true,false].map((isPublic) => {
    return new Promise(function(resolve, reject) {
      do_put(
        base_url + (isPublic ? '-public' : ''),
        null,
        function(resp) { resolve(user) },
        reject,
        'create ' + (isPublic ? 'public' : '') + ' db'
      );
    });
  })).then(([user]) => user)      //FIXME: I hope this doesn't swallow the error. error should propagate
};

var add_security_doc = function(user) {
  const base_url = 'http://' + CONFIG['httpd'].bind_address + ':' + CONFIG['httpd'].port + '/' + user[CONFIG[section].namespace].dbname;
  return Promise.all([true,false].map((isPublic) => {
    return new Promise(function(resolve, reject) {
      do_put(
        base_url + (isPublic ? '-public' : '') + '/_security',
        {
          members: {
            names: isPublic ? [] : [user.name],
            roles: []
          },
          admins: {
            names: [SYSTEM_DB_NAME],
            roles: [SYSTEM_DB_NAME]
          }
        }, 
        function(resp) { resolve(user) },
        reject,
        'add ' + (isPublic ? 'public' : '') + ' security doc'
      );
    });
  })).then(([user]) => user);
};

var add_doc_update_ddoc = function(user) {
  const base_url = 'http://' + CONFIG['httpd'].bind_address + ':' + CONFIG['httpd'].port + '/' + user[CONFIG[section].namespace].dbname;

  return Promise.all([true,false].map((isPublic) => {
    return new Promise(function(resolve, reject) {
        do_put(
          base_url + (isPublic ? '-public' : '') + '/_design/app',
          ddoc, 
          function(resp) { resolve(user) },
          reject,
          'add ' + (isPublic ? 'public' : '') + ' views'
        );
      });
  })).then(([user]) => user);
}

var replicate_example_docs = function(user) {
  const base_url = 'http://' + CONFIG['httpd'].bind_address + ':' + CONFIG['httpd'].port;

  const source = SYSTEM_DB_NAME + "-public";
  const target = user[CONFIG[section].namespace].dbname; 

  return new Promise(function(resolve, reject) {

    if(source === target){
      return resolve(user);  //skip: we don't need ot copy to himself
    }

    request.get(
      {
        url: base_url + '/' + source + '/_design/app/_view/byExampleProject',
        auth: {
          user: CONFIG[section].admin_username,
          pass: CONFIG[section].admin_password,
          sendImmediately : true
        }
      }, 
      function(error, response, body) {
        if (error) {
          info && (error.info = info);
          reject(error);
          return;
        }
        
        var b = JSON.parse(body);
        var doc_ids = b.rows.map( function(r){ return r.id; } );

        var replicate_doc = {
          source,
          target,
          doc_ids
        };

        const logMessage = 'replicate example projects from ' + source + ' to ' + target;
        do_post(
          base_url + '/_replicate',
          replicate_doc, 
          function(resp) { 
            log(logMessage);
            resolve(user) 
          },
          reject,
          logMessage
        );
      }
    );
  });
}

var server = http.createServer(function(req, resp) {
  if(req.method === 'POST'){
    let s = '';
    req.on('data',function(data){
      s+=data;
    });
    req.on('end',function(){
      const data = JSON.parse(s);

      create_user(data)
        .then(create_database)
        .then(replicate_example_docs)
        .then(add_security_doc)
        .then(add_doc_update_ddoc)
        .then(function(d) {
          // do not return the password
          delete d['password'];
          
          resp.writeHead(200, { 'Content-Type': 'application/json' });
          resp.end(JSON.stringify(d));
          log('provisioned user: '+d.name+', db: '+d[CONFIG[section].namespace].dbname);
        })
        .catch(function(err) {
          resp.writeHead(403, { 'Content-Type': 'application/json' });
          resp.end(JSON.stringify(err));
          log('error provisioning user: '+JSON.stringify(err));
        });
    });
  } else {
    resp.writeHead(405, { 'Content-Type': 'application/json' });
    resp.end(JSON.stringify({ok : false, reason : 'The server only supports POST'}));
  } 
});

// OS daemon stdin listener

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// ugly, but I had trouble using promises for this sequence
rl.question(JSON.stringify(['get', 'httpd']) + '\n', function(answer) {
  //log(JSON.stringify(['get', 'httpd']) + ': answer '+ answer);
  CONFIG['httpd'] = JSON.parse(answer);
  
  // check the bind address and port
  var bind_address = CONFIG['httpd'].bind_address;
  if (!bind_address || bind_address === '0.0.0.0') {
    CONFIG['httpd'].bind_address = '127.0.0.1'
  }
  var port = CONFIG['httpd'].port;
  if (!port) {
    CONFIG['httpd'].port = '5984';
  }
  log('connecting to CouchDB on ' + CONFIG['httpd'].bind_address + ':' + CONFIG['httpd'].port);
  
  rl.question(JSON.stringify(['get', 'couch_httpd_auth']) + '\n', function(answer) {
    //log(JSON.stringify(['get', 'couch_httpd_auth']) + ': answer '+ answer);
    CONFIG['couch_httpd_auth'] = JSON.parse(answer);
    
    rl.question(JSON.stringify(['get', section]) + '\n', function(answer) {
      //log(JSON.stringify(['get', section) + ': answer '+ answer);
      CONFIG[section] = JSON.parse(answer);
      
      if (CONFIG[section].port) {
        server.listen(parseInt(CONFIG[section].port), function() {
          log('provisioning service listening on port '+CONFIG[section].port);
        });
      }
      else {
        log('missing required parameter "port" in config section '+section);
      }
    })
  });
});

rl.on('close', function() {
  // Close event fired, stop http server')
  server.close();
});
