/* global emit */
const ddoc = {
  _id: '_design/app', 
  views: {
    byProject : {
      map : (function(doc) {
        if (doc.type === 'project') {  
          emit(doc._id,{
            name : doc.name,
            description : doc.description,
            lastModified: doc.lastModified,
            _rev: doc._rev
          }) 
        } 
      }).toString()
    },
    byExampleProject : {
      map : (function(doc) {
        if (doc.type === 'project.example') {  
          emit(doc._id, {
            name : doc.name,
            description : doc.description
          }) 
        } 
      }).toString()
    }
  },
  validate_doc_update: function(new_doc, old_doc, userCtx, securityCtx) { 
    if(
      !(
        //does userCtx.name exist in securityCtx.members.names or securityCtx.admins.names?
        (securityCtx.members.names.indexOf(userCtx.name) > -1 ||
          securityCtx.admins.names.indexOf(userCtx.name) > -1) ||

        //does there exist a role in userCtx.roles such that role is in securityCtx.members.roles or securityCtx.admins.roles?
        (
          userCtx.roles.some(function(role){
            return (securityCtx.members.roles.indexOf(role) > -1 ||
              securityCtx.admins.roles.indexOf(role) > -1 ) 
          })
        ) ||
        //or is the user an admin?
        userCtx.roles.indexOf('_admin') > -1
      )
    ){
      throw({forbidden: 'Not Authorized'}); 
    } 
  }
}

module.exports = ddoc;

